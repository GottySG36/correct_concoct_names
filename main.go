// Simple utility to correct contigs names since errors occur after merging bins

package main

import (
    "bufio"
    "flag"
    "fmt"
    "log"
    "os"
    "regexp"
    "strings"

    "bitbucket.org/GottySG36/tree"
)

type Line struct {
    Contig  string
    Bin     string
}

func (b Bins) ToCsv(f *os.File, sep string) {
//    fmt.Fprintf(f, "contig_id%vcluster_id\n", sep)
    for _, l := range b {
        fmt.Fprintf(f, "%v%v%v\n", l.Contig, sep, l.Bin)
    }
}

type Bins []*Line

var bad  = flag.String("b", "", "Input file to correct")
var good = flag.String("g", "", "Input file containing the correct names")
var outp = flag.String("o", "", "Output file")
var exact = flag.Bool("exact", false, "Will only look for exact matches and not partial ones")
var regex = flag.String("regex", `NODE_\d+_length_\d+_cov_\d+\.\d+`, "Regex to use to matchthe fixed portion of the header name, used to build the search tree")


func main() {
    flag.Parse()
    var reg = regexp.MustCompile(*regex)
    // Opening file to correct
    b, err := os.Open(*bad)
    defer b.Close()
    if err != nil {
        log.Fatalf("Error -:- Opening file : %v\n", err)
    }

    // Opening file containing correct names
    g, err := os.Open(*good)
    defer g.Close()
    if err != nil {
        log.Fatalf("Error -:- Opening file : %v\n", err)
    }

    searchTree := &tree.Tree{}

    s := bufio.NewScanner(g)
    s.Scan() // Skip first line (header)
    for s.Scan() {
        l := strings.Split(s.Text(), ",")
        name := reg.FindAllString(l[0], 1)[0]
        searchTree.Insert(&tree.Node{Name:name})
    }

    output := make(Bins, 0)

    s = bufio.NewScanner(b)
    s.Scan() // Skip first line (header)
    for s.Scan() {
        l := strings.Split(s.Text(), ",")
        contig, bin := l[0], l[1]
        if res, ok := searchTree.Find(contig, *exact); ok != nil {
            log.Printf("INFO -:- %v\n", ok)
        } else {
            output = append(output, &Line{Contig:res, Bin:fmt.Sprintf("Bin_%v", bin)})
        }
    }

    o, err := os.Create(*outp)
    defer o.Close()
    if err != nil {
        log.Fatalf("Error -:- %v\n", err)
    }

    output.ToCsv(o, "\t")

}
